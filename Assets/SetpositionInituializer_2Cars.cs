using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetpositionInituializer_2Cars : MonoBehaviour
{
    [SerializeField] private Transform left, right, left1, right2;
    void Start()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();

        float worldScreenHeight = Camera.main.orthographicSize * 2;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        var temp = (worldScreenWidth) / 8;
        var xOffset = (worldScreenWidth) / 4;
        left.localPosition = new Vector3(-(temp + xOffset), left.localPosition.y, left.localPosition.z);
        
        left1.localPosition = new Vector3(-(temp ), left.localPosition.y, left.localPosition.z);
       
        right.localPosition = new Vector3((temp ), right.localPosition.y, right.localPosition.z);

        right2.localPosition = new Vector3((temp + (xOffset)), right.localPosition.y, right.localPosition.z);
    }
}
