using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager_2cars : MonoBehaviour
{
    public GameObject gameOverPanel,mainMenuPanel,spawn;
    [SerializeField] private GameManager_2Cars gameManager;
    [SerializeField] private input_2Cars left, right;
    public void Play()
    {
        mainMenuPanel.SetActive(false);
        GameManager_2Cars.isplaying = true;
    }
    public void Retry()
    {
         DestroyHurdles();
        //SceneManager.LoadScene("game");
    }
    private void DestroyHurdles()
    {
        for (int i = 0; i < spawn.transform.childCount; i++)
        {
            Destroy(spawn.transform.GetChild(i).gameObject);
        }
        left.ResetCarPosition();
        right.ResetCarPosition();
        gameManager.ResetScore();
        gameOverPanel.SetActive(false);
    }
    public void Home()
    {
        SceneManager.LoadScene(0);
    }
}
