using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetLinesPosition_2Cars : MonoBehaviour
{
  [SerializeField]private Transform left,right;
   
    void Start()
    {
        SetPosition();
    }
    public void SetPosition()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();

        float worldScreenHeight = Camera.main.orthographicSize * 2;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        left.localPosition = new Vector3(-(worldScreenWidth) / 4, left.localPosition.y, left.localPosition.z);
        right.localPosition = new Vector3((worldScreenWidth) / 4, right.localPosition.y, right.localPosition.z);
    }

}
