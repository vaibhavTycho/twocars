using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject_2Cars : MonoBehaviour
{
   [SerializeField] private float speed,offset,yPos;
    void Start()
    {
       // speed = PlayerPrefs.GetFloat("speed");
    }
    void Update()
    {
       // speed += Time.deltaTime / 10;
        //PlayerPrefs.SetFloat("speed", speed);
        this.transform.Translate(Vector3.down * speed * Time.deltaTime);
        if(transform.position.y<yPos)
        {
            transform.position = transform.position + Vector3.up * offset;
        }
    }
}
