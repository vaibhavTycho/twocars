﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class initializer_2Cars : MonoBehaviour {
    public enum category
    {
        A, B, C, D
    }
    public category obj;
    public GameObject[] objects;
    public GameObject[] copyclone;
    private GameObject clones;
    private float tempt;
    private int destructor;
    public float final; 
    public float initial;
    public float counter;

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, 0.6f);
    }


	void Start () {
        counter = 1f;
        initial = 2f;
        final = 0f;
    }
    void Update()
    {
        if (GameManager_2Cars.isplaying)
        {
            if (Time.time > tempt)
            {
                if (obj == category.A && GameManager_2Cars.c1 <= 0f)
                {
                    if (counter * Time.deltaTime < Random.value)
                    {
                        int spawnnumber = Random.Range(0, 2);
                        GameObject clones = Instantiate(objects[spawnnumber]) as GameObject;
                        clones.transform.position = this.transform.position;
                        clones.transform.parent = GameObject.Find("Spawns").transform;
                    }
                    tempt = Time.time + initial;
                    GameManager_2Cars.c1 = 4f;
                    GameManager_2Cars.c2 = 2f;
                    //Debug.Log(GameManager.c1);
                }
                if (obj == category.B && GameManager_2Cars.c2 <= 0f)
                {
                    if (counter * Time.deltaTime < Random.value)
                    {
                        int spawnnumber = Random.Range(0, 2);
                        GameObject clones = Instantiate(objects[spawnnumber]) as GameObject;
                        clones.transform.position = this.transform.position;
                        clones.transform.parent = GameObject.Find("Spawns").transform;
                    }
                    tempt = Time.time + initial;
                    GameManager_2Cars.c1 = 2f;
                    //Debug.Log(GameManager.c1);
                }
                if (obj == category.C && GameManager_2Cars.c3 <= 0f)
                {
                    if (counter * Time.deltaTime < Random.value)
                    {
                        int spawnnumber = Random.Range(0, 2);
                        GameObject clones = Instantiate(objects[spawnnumber]) as GameObject;
                        clones.transform.position = this.transform.position;
                        clones.transform.parent = GameObject.Find("Spawns").transform;
                    }
                    tempt = Time.time + initial;
                    GameManager_2Cars.c4 = 2f;
                    GameManager_2Cars.c3 = 4f;
                    //Debug.Log(GameManager.c1);
                }
                if (obj == category.D && GameManager_2Cars.c4 <= 0f)
                {
                    if (counter * Time.deltaTime < Random.value)
                    {
                        int spawnnumber = Random.Range(0, 2);
                        GameObject clones = Instantiate(objects[spawnnumber]) as GameObject;
                        clones.transform.position = this.transform.position;
                        clones.transform.parent = GameObject.Find("Spawns").transform;
                    }
                    tempt = Time.time + initial;
                    GameManager_2Cars.c3 = 2f;
                    //Debug.Log(GameManager.c1);
                }
            }
        }
    }
}
