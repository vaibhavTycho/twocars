﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class input_2Cars : MonoBehaviour {
   
    public bool lane1;
    public bool lane2;
   [SerializeField] private float xPosOffset,RotationOffset;
    private Vector3 t_Pos;
    private float car_Temp;
    private void Awake()
    {
        Input.multiTouchEnabled = true;
    }
    private void Start()
    {
        ResetCarPosition();
    }
    public void ResetCarPosition()
    {
        float worldScreenHeight = Camera.main.orthographicSize * 2;
        float worldScreenWidth = (worldScreenHeight / Screen.height) * Screen.width;

        xPosOffset = (worldScreenWidth) / 4;

        car_Temp = (worldScreenWidth) / 8;
        car_Temp = gameObject.tag == "left" ? -(car_Temp + xPosOffset) : car_Temp;
        transform.localPosition = new Vector3(car_Temp, transform.localPosition.y, transform.localPosition.z);
        lane1 = true;lane2 = false;
        t_Pos = transform.position;
    }
    // Update is called once per frame
    void Update() {
        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch touch = Input.GetTouch(i);
            if (touch.phase == TouchPhase.Ended  && GameManager_2Cars.isplaying)
            {
                
                if (/*Input.mousePosition.x*/touch.position.x < Screen.width / 2)
                {
                    if (this.gameObject.tag == "left")
                    {
                        if (lane1 && !lane2)
                        {
                            lane1 = false;
                            lane2 = true;
                            //t_Pos = new Vector3(transform.position.x+ xPosOffset, transform.position.y);

                            t_Pos = new Vector3(car_Temp + xPosOffset, transform.position.y);
                            //  this.transform.Translate(Vector3.right * 2f);
                        }
                        else if (lane2 && !lane1)
                        {
                            lane1 = true;
                            lane2 = false;
                            //  t_Pos = new Vector3(transform.position.x -xPosOffset, transform.position.y);
                            t_Pos = new Vector3(car_Temp, transform.position.y);
                            //  this.transform.Translate(Vector3.left * 2f);
                        }
                    }
                }
            }

            if (touch.phase == TouchPhase.Ended  && GameManager_2Cars.isplaying)
            {
                if (touch.position.x > Screen.width / 2)
                {
                    if (this.gameObject.tag == "right")
                    {
                        if (lane1 && !lane2)
                        {
                            lane1 = false;
                            lane2 = true;
                            //t_Pos = new Vector3(transform.position.x + xPosOffset, transform.position.y);
                            t_Pos = new Vector3(car_Temp + xPosOffset, transform.position.y);
                            // this.transform.Translate(Vector3.right * 2f);
                        }
                        else if (lane2 && !lane1)
                        {
                            lane1 = true;
                            lane2 = false;
                            // t_Pos = new Vector3(transform.position.x -xPosOffset, transform.position.y);
                            t_Pos = new Vector3(car_Temp, transform.position.y);
                            // this.transform.Translate(Vector3.left * 2f );
                        }
                    }
                }
            }
        }
        MoveCar(t_Pos,RotationOffset);
    }
    
    private void MoveCar(Vector3 targetPos,float r_Offset)
    {
        if (transform.position != targetPos)
        {
            transform.position = Vector3.Lerp(transform.position, targetPos, 0.1f);
            transform.rotation = Quaternion.FromToRotation(targetPos,transform.position);
        }
    }
}
