﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.UI;

public class GameManager_2Cars : MonoBehaviour {
    public Text textfield,highscoretxt;
    public static float c1,c2,c3,c4;
    public static int score,highscore;
    public static bool isplaying = false;
    public  void points()
    {
        score++;
        updatetext();
    }
    // Use this for initialization
    void Start () {
        Init();
    }
    private void Init()
    {
        c1 = c2 = c3 = c4 = 2f;
        score = 0;
        highscore =  PlayerPrefs.GetInt("high");
        highscoretxt.text = highscore.ToString();
        PlayerPrefs.SetFloat("speed", 2f);
       
    }
    // Update is called once per frame
    void Update () {
        c1 = Mathf.Clamp(c1 - Time.deltaTime, 0f, 2f);
        c2 = Mathf.Clamp(c2 - Time.deltaTime, 0f, 2f);
        c3 = Mathf.Clamp(c3 - Time.deltaTime, 0f, 2f);
        c4 = Mathf.Clamp(c4 - Time.deltaTime, 0f, 2f);
      //  Debug.Log(i);
     //   Debug.Log(j);
        if (highscore < score) 
        {
            highscore = score;
            PlayerPrefs.SetInt("high", highscore); 
            highscoretxt.text = highscore.ToString();
        }
        /*
           Debug.Log(c1);
           Debug.Log(c2);
           Debug.Log(c3);
           Debug.Log(c4);
       */
    }
    public  void updatetext()
     {
        textfield.text = score.ToString();
        highscoretxt.text = highscore.ToString();
     }
    public void ResetScore()
    {
        Init();
        updatetext();
        
        isplaying = true;
    }
}