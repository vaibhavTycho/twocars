﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scripts_2Cars : MonoBehaviour
{
    public static float speed,offset;
    [SerializeField] private bool line;
    [SerializeField] private GameManager_2Cars gm_car;
    private GameObject gameoverPanel;
    void Start()
    {
        speed = PlayerPrefs.GetFloat("speed");
        var uimanger = GameObject.Find("Canvas").GetComponent<UIManager_2cars>();
        gameoverPanel = uimanger.gameOverPanel;

    }
    void Update()
    {
        if (GameManager_2Cars.isplaying)
        {
            speed += Time.deltaTime / 10;
            PlayerPrefs.SetFloat("speed", speed);
            this.transform.Translate(Vector3.down * speed * Time.deltaTime);
          
        }
    }
    private void gameover()
    {
        Debug.Log("gameover");
        // SceneManager.LoadScene("game");
        GameManager_2Cars.isplaying = false;
        gameoverPanel.SetActive(true);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (((this.gameObject.tag == "BlueSquare" || this.gameObject.tag == "RedSquare") && (collision.gameObject.tag == "left" || collision.gameObject.tag == "right")) || ((this.gameObject.tag == "BlueCircle" || this.gameObject.tag == "RedCircle") && collision.gameObject.tag == "Destroyer"))
        {
            Debug.Log(collision.gameObject.tag +"  "+this.tag);
            gameover();
        }
        if ((this.gameObject.tag == "BlueCircle" || this.gameObject.tag == "RedCircle") && (collision.gameObject.tag == "left" || collision.gameObject.tag=="right"))
        {
            if(gm_car==null)
            {
                gm_car = GameObject.Find("GameManager").GetComponent<GameManager_2Cars>();
            }
            gm_car.points();
            Destroy(this.gameObject);
        }
        if (collision.gameObject.tag == "Destroyer")
        {
            Destroy(this.gameObject);
        }
    }
}